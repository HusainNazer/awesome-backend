const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    cart: {
        type: Array,
        default: []
    },
    favorites: {
        type: Array,
        default: []
    },
    card_details: {
        type: Array,
        default: []
    },
    shipping_address: {
        type: Array,
        default: []
    },
    pickup_address: {
        type: Array,
        default: []
    }
});

mongoose.model("User", userSchema);

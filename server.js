//import
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const { MONGOURI } = require("./keys");

//app config
const app = express();
const port = process.env.PORT || 9000;

console.log("PROCESS ENV", process.env.NODE_ENV)

//DB config
const connection_url = MONGOURI;
mongoose.connect(connection_url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

mongoose.connection.on("connected", () => {
    console.log("db connected");
});

mongoose.connection.on("error", (err) => {
    console.log("err connecting", err);
});

require("./models/user");
require("./models/post");

app.use(express.json());
app.use(cors());
app.use(require("./routes/auth"));
app.use(require("./routes/post"));
app.use(require("./routes/payment"));
app.use(require("./routes/card"));
app.use(require("./routes/cart"));
app.use(require("./routes/favourites"));
app.use(require("./routes/pickup_address"));
app.use(require("./routes/shipping_address"));

// ????

//listener
app.listen(port, () => console.log(`Listening to localhost:${port}`));

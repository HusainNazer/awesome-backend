const { RAZORPAY_ID, RAZORPAY_SECRET } = require("../keys");
const Razorpay = require("razorpay")
const crypto = require("crypto");
const express = require("express");
const { json } = require("express");
const router = express.Router();

// const stripe = require("stripe")(SK_LIVE);

var instance = new Razorpay({
  key_id: RAZORPAY_ID,
  key_secret: RAZORPAY_SECRET,
});

router.post('/create-payment-order', (req, res) => {
  const {product_amount} = req.body

  if(!product_amount) {
    return res.status(400).json({ error: "please add all the fields" });
  }

  var options = {
    amount: product_amount * 100,
    currency: "INR",
  };

  instance.orders.create(options)
    .then((order) => res.json({order: order}))
    .catch(err => console.log(err))
})

router.post('/verify-signature', (req, res) => {
  const {order_id, razorpay_payment_id, razorpay_signature} = req.body
  const hmac = crypto.createHmac('sha256', RAZORPAY_SECRET);

  hmac.update(order_id + "|" + razorpay_payment_id);
  let generatedSignature = hmac.digest('hex');

  let isSignatureValid = generatedSignature == razorpay_signature;
  res.json({response: isSignatureValid})
})

router.get('/test', (req, res) => {
  instance.orders.all().then(data => console.log(data)).catch(err => console.log(err))
  // instance.orders.fetchall().then((data) => console.log(data))
})

// router.post('/create-session', (req, res) => {
//     const {product_title, product_images, product_amount, product_quantity} = req.body
//     stripe.checkout.sessions.create({
//       payment_method_types: ['card'],
//       line_items: [
//         {
//           price_data: {
//             currency: 'inr',
//             product_data: {
//               name: product_title,
//               images: product_images,
//             },
//             unit_amount: product_amount,
//           },
//           quantity: product_quantity,
//         },
//       ],
//       mode: 'payment',
//       success_url: 'http://localhost:3000/checkout?success=true',
//       cancel_url: 'http://localhost:3000/checkout?canceled=true',
//     })
//     .then((response) => res.json({session: response}))
//     .catch((err) => console.log(err))
//   });

// router.post('/create-payment-intent', (req, res) => {
//   const {product_amount} = req.body
//   stripe.paymentIntents.create({
//     amount: product_amount * 100,
//     currency: 'inr',
//     payment_method_types: ['card'],
//   })
//   .then(result => {
//     res.json({response: result})
//   })
//   .catch(err => console.log(err))
// })
  

module.exports = router;


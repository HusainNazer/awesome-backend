const express = require("express");
const router = express.Router();
const mongoose = require("mongoose")
const User = mongoose.model("User");
const requireLogin = require("../middlewares/requireLogin");

router.post('/card_details/add', requireLogin, (req, res) => {
    const { card_number, card_name, exp_month, exp_year } = req.body;
    if (!card_number || !card_name || !exp_month || !exp_year) {
        return res.status(400).json({ error: "please add all fields" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.card_details.forEach((item) => {
            if (item.card_number == card_number) {
                duplicate = true;
            }
        })

        if (duplicate) {
            res.status(200).json({message: 'card already added'})
        } else {
            User.findOneAndUpdate(
                { _id: req.user._id },
                {
                    $push: {
                        card_details: {
                            card_number: card_number,
                            card_name: card_name,
                            exp_month: exp_month,
                            exp_year: exp_year
                        }
                    }
                },
                { new: true },
                (err, userInfo) => {
                    if (err) return res.json({ success: false, err });
                    res.status(200).json({card_details: userInfo.card_details})
                }
            )
        }
    })
});

router.get("/card_details/list", requireLogin, (req, res) => {
    res.json({card_details: req.user.card_details})
});

router.post('/card_details/delete', requireLogin, (req, res) => {
    const { card_number } = req.body;
    if (!card_number) {
        return res.status(400).json({ error: "add card_number" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.card_details.forEach((item) => {
            if (item.card_number == card_number) {
                duplicate = true;
            }
        })

        if (duplicate) {
            User.findOneAndUpdate(
                { _id: req.user._id },
                { $pull: { card_details: { card_number: card_number } } },
                { new: true }
              )
                .then(() => res.status(200).json({message: 'card removed from cards list'}))
                .catch(err => console.log(err));
        } else {
            res.status(200).json({message: 'card not found in cards list'})
        }
    })
});

module.exports = router;

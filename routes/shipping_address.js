const express = require("express");
const router = express.Router();
const mongoose = require("mongoose")
const User = mongoose.model("User");
const requireLogin = require("../middlewares/requireLogin");
const ObjectID = require('mongodb').ObjectID

router.post('/shipping_address/add', requireLogin, (req, res) => {
    const newObjectId = mongoose.Types.ObjectId();
    const { city, pincode, country, state, address, ph_number } = req.body;
    if (!city || !pincode || !country || !state || !address || !ph_number) {
        return res.status(400).json({ error: "please add all fields" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.shipping_address.forEach((item) => {
            if (item.pincode == pincode) {
                duplicate = true;
            }
        })

        if (duplicate) {
            res.status(200).json({message: 'address already added'})
        } else {
            User.findOneAndUpdate(
                { _id: req.user._id },
                {
                    $push: {
                        shipping_address: {
                            id: newObjectId,
                            city: city,
                            pincode: pincode,
                            country: country,
                            state: state,
                            address: address,
                            ph_number: ph_number
                        }
                    }
                },
                { new: true },
                (err, userInfo) => {
                    if (err) return res.json({ success: false, err });
                    res.status(200).json({shipping_address: userInfo.shipping_address})
                }
            )
        }
    })
});

router.get("/shipping_address/list", requireLogin, (req, res) => {
    res.json({shipping_address: req.user.shipping_address})
});

router.post('/shipping_address/delete', requireLogin, (req, res) => {
    const { addressId } = req.body;
    if (!addressId) {
        return res.status(400).json({ error: "add addressId" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.shipping_address.forEach((item) => {
            if (item.id == addressId) {
                duplicate = true;
            }
        })

        if (duplicate) {
            User.findOneAndUpdate(
                { _id: req.user._id },
                { $pull: { shipping_address: { id: ObjectID(addressId) } } },
                { new: true }
              )
                .then(() => res.status(200).json({message: 'address removed from address list'}))
                .catch(err => console.log(err));
        } else {
            res.status(200).json({message: 'address not found in address list'})
        }
    })
});

module.exports = router;

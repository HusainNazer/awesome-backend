const express = require("express");
const router = express.Router();
const mongoose = require("mongoose")
const User = mongoose.model("User");
const Post = mongoose.model("Post");
const requireLogin = require("../middlewares/requireLogin");

router.post('/favorites/add', requireLogin, (req, res) => {
    const { productId } = req.body;
    if (!productId) {
        return res.status(400).json({ error: "add productId" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.favorites.forEach((item) => {
            if (item.id == productId) {
                duplicate = true;
            }
        })

        if (duplicate) {
            res.status(200).json({message: 'already in favorites'})
        } else {
            User.findOneAndUpdate(
                { _id: req.user._id },
                {
                    $push: {
                        favorites: {
                            id: productId,
                            date: Date.now()
                        }
                    }
                },
                { new: true },
                (err, userInfo) => {
                    if (err) return res.json({ success: false, err });
                    res.status(200).json({favorites: userInfo.favorites})
                }
            )
        }
    })
});

router.get("/favorites/list", requireLogin, (req, res) => {
    req.user.favorites.forEach((item) => {
        Post.findOne({ _id: item.id })
            .then((post) => {
                if(post == null) {
                    User.findOneAndUpdate(
                        { _id: req.user._id },
                        { $pull: { favorites: { id: item.id } } },
                        { new: true }
                      )
                        .then(() => console.log('removed junk from favourites'))
                        .catch(err => console.log(err));
                }
            })
            .catch((err) => {
                console.log('err', err);
            });
    })
    res.json({favorites: req.user.favorites})
});

router.post('/favorites/delete', requireLogin, (req, res) => {
    const { productId } = req.body;
    if (!productId) {
        return res.status(400).json({ error: "add productId" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.favorites.forEach((item) => {
            if (item.id == productId) {
                duplicate = true;
            }
        })

        if (duplicate) {
            User.findOneAndUpdate(
                { _id: req.user._id },
                { $pull: { favorites: { id: productId } } },
                { new: true }
              )
                .then(() => res.status(200).json({message: 'product removed from favorites'}))
                .catch(err => console.log(err));
        } else {
            res.status(200).json({message: 'product not found in favorites'})
        }
    })
});

module.exports = router;
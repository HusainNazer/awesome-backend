const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Post = mongoose.model("Post");
const requireLogin = require("../middlewares/requireLogin");

router.post("/createpost", requireLogin, (req, res) => {
    const { title, price, photo, size, description, pickup_location, dimensions} = req.body;
    if (
        !title ||
        !price ||
        !photo ||
        !size ||
        !description ||
        !pickup_location ||
        !dimensions
    ) {
        return res.status(400).json({ error: "please add all the fields" });
    }
    req.user.password = undefined;
    const post = new Post({
        title: title,
        price: price,
        photo: photo,
        size: size,
        description: description,
        pickup_location: pickup_location,
        dimensions: dimensions,
        postedBy: req.user,
    });
    post.save()
        .then((result) => {
            res.json({ post: result });
        })
        .catch((err) => console.log(err));
});

router.get("/allposts", (req, res) => {
    Post.find()
        .populate("postedBy", "_id name email")
        .then((posts) => {
            res.json({ posts: posts });
        })
        .catch((err) => {
            console.log(err);
        });
});

router.post("/getpost", (req, res) => {
    const { id } = req.body;
    Post.findOne({ _id: id })
        .populate("postedBy", "_id name email")
        .then((post) => {
            res.json({ post: post });
        })
        .catch((err) => {
            console.log(err);
        });
});

router.get("/myposts", requireLogin, (req, res) => {
    Post.find({ postedBy: req.user._id })
        .populate("postedBy", "_id name email")
        .then((myposts) => {
            res.json({ myposts: myposts });
        })
        .catch((err) => {
            console.log(err);
        });
});

module.exports = router;

const express = require("express");
const router = express.Router();
const mongoose = require("mongoose")
const User = mongoose.model("User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("../keys");
const requireLogin = require("../middlewares/requireLogin");

router.post("/signup", (req, res) => {
    const { name, email, password } = req.body;
    if (!name || !email || !password) {
        return res.status(400).json({ error: "please add all the fields" });
    }
    User.findOne({ email: email })
        .then((savedUser) => {
            if (savedUser) {
                return res.status(400).json({ error: "user already exists" });
            }
            bcrypt.hash(password, 12).then((hashedPassword) => {
                const user = new User({
                    name: name,
                    email: email,
                    password: hashedPassword,
                    type: 'user'
                });

                user.save()
                    .then((user) => {
                        const token = jwt.sign({ _id: user._id }, JWT_SECRET);
                        res.json({
                            message: "saved successfully",
                            token: token,
                            id: user._id,
                            name: user.name,
                            email: user.email,
                            type: user.type,
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            });
        })
        .then((err) => {
            console.log(err);
        });
});

router.post("/signup/seller", (req, res) => {
    const { name, email, password } = req.body;
    if (!name || !email || !password) {
        return res.status(400).json({ error: "please add all the fields" });
    }
    User.findOne({ email: email })
        .then((savedUser) => {
            if (savedUser) {
                return res.status(400).json({ error: "user already exists" });
            }
            bcrypt.hash(password, 12).then((hashedPassword) => {
                const user = new User({
                    name: name,
                    email: email,
                    password: hashedPassword,
                    type: 'seller'
                });

                user.save()
                    .then((user) => {
                        const token = jwt.sign({ _id: user._id }, JWT_SECRET);
                        res.json({
                            message: "saved successfully",
                            token: token,
                            id: user._id,
                            name: user.name,
                            email: user.email,
                            type: user.type,
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            });
        })
        .then((err) => {
            console.log(err);
        });
});

router.post("/signin", (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
        return res.status(400).json({ error: "please add email and password" });
    }
    User.findOne({ email: email }).then((savedUser) => {
        if (!savedUser) {
            return res.status(400).json({ error: "Invalid email or password" });
        }
        bcrypt
            .compare(password, savedUser.password)
            .then((isMatching) => {
                if (isMatching) {
                    const token = jwt.sign({ _id: savedUser._id }, JWT_SECRET);
                    res.json({
                        message: "successfully signed in",
                        token: token,
                        id: savedUser._id,
                        name: savedUser.name,
                        email: savedUser.email,
                        type: savedUser.type,
                    });
                } else {
                    return res
                        .status(400)
                        .json({ error: "Invalid email or password" });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    });
});

router.post('/ph_number/add', requireLogin, (req, res) => {
    const { ph_number } = req.body

    if(!ph_number) {
        return res.status(400).json({ error: "add phone number" });
    }

    User.findOneAndUpdate(
        { _id: req.user._id },
        {
            $set: {
                ph_number: ph_number
            }
        },
        { new: true },
        (err, userInfo) => {
            if (err) return res.json({ success: false, err });
            res.status(200).json({ph_number: userInfo.ph_number})
        }
    )
    
})

module.exports = router;
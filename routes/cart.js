const express = require("express");
const router = express.Router();
const mongoose = require("mongoose")
const User = mongoose.model("User");
const Post = mongoose.model("Post");
const requireLogin = require("../middlewares/requireLogin");

router.post('/cart/add', requireLogin, (req, res) => {
    const { productId, quantity } = req.body;
    if (!productId || !quantity) {
        return res.status(400).json({ error: "add productId and quantity" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.cart.forEach((item) => {
            if (item.id == productId) {
                duplicate = true;
            }
        })

        if (duplicate) {
            res.status(200).json({message: 'already in cart'})
        } else {
            User.findOneAndUpdate(
                { _id: req.user._id },
                {
                    $push: {
                        cart: {
                            id: productId,
                            quantity: quantity,
                            date: Date.now()
                        }
                    }
                },
                { new: true },
                (err, userInfo) => {
                    if (err) return res.json({ success: false, err });
                    res.status(200).json(userInfo.cart)
                }
            )
        }
    })
});

router.post('/cart/delete', requireLogin, (req, res) => {
    const { productId } = req.body;
    if (!productId) {
        return res.status(400).json({ error: "add productId" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.cart.forEach((item) => {
            if (item.id == productId) {
                duplicate = true;
            }
        })

        if (duplicate) {
            User.findOneAndUpdate(
                { _id: req.user._id },
                { $pull: { cart: { id: productId } } },
                { new: true }
              )
                .then(() => res.status(200).json({message: 'product removed from cart'}))
                .catch(err => console.log(err));
        } else {
            res.status(200).json({message: 'product not found in cart'})
        }
    })
});

router.get("/cart/list", requireLogin, (req, res) => {
    req.user.cart.forEach((item) => {
        Post.findOne({ _id: item.id })
            .then((post) => {
                if(post == null) {
                    User.findOneAndUpdate(
                        { _id: req.user._id },
                        { $pull: { cart: { id: item.id } } },
                        { new: true }
                        )
                        .then(() => console.log('removed junk from cart'))
                        .catch(err => console.log(err));
                }
            })
            .catch((err) => {
                console.log('err', err);
            });
    })
    res.json({cart: req.user.cart})
});

module.exports = router;
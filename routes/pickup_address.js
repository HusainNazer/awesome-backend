const express = require("express");
const router = express.Router();
const mongoose = require("mongoose")
const User = mongoose.model("User");
const requireLogin = require("../middlewares/requireLogin");
const ObjectID = require('mongodb').ObjectID
const axios = require('axios');

router.post('/pickup_address/add', requireLogin, (req, res) => {
    const newObjectId = mongoose.Types.ObjectId();
    const { pickup_location, name, email, phone, address, city, state, country, pin_code } = req.body;
    if (!pickup_location || !name || !email || !phone || !address || !city || !state || !country || !pin_code) {
        return res.status(400).json({ error: "please add all fields" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.pickup_address.forEach((item) => {
            if (item.pickup_location == pickup_location) {
                duplicate = true;
            }
        })

        if (duplicate) {
            res.status(200).json({message: 'address already added'})
        } else {
            User.findOneAndUpdate(
                { _id: req.user._id },
                {
                    $push: {
                        pickup_address: {
                            id: newObjectId,
                            pickup_location: pickup_location,
                            name: name,
                            email: email,
                            phone: phone,
                            address: address,
                            city: city,
                            state: state,
                            country: country,
                            pin_code: pin_code
                        }
                    }
                },
                { new: true },
                (err, userInfo) => {
                    if (err) return res.json({ success: false, err });
                    res.status(200).json({pickup_address: userInfo.pickup_address})
                }
            )
        }
    })
});

router.get("/pickup_address/list", requireLogin, (req, res) => {
    res.json({pickup_address: req.user.pickup_address})
});

router.post('/pickup_address/delete', requireLogin, (req, res) => {
    const { addressId } = req.body;
    if (!addressId) {
        return res.status(400).json({ error: "add addressId" });
    }

    User.findOne({ _id: req.user._id }, (err, userInfo) => {
        let duplicate = false;

        userInfo.pickup_address.forEach((item) => {
            if (item.id == addressId) {
                duplicate = true;
            }
        })

        if (duplicate) {
            User.findOneAndUpdate(
                { _id: req.user._id },
                { $pull: { pickup_address: { id: ObjectID(addressId) } } },
                { new: true }
              )
                .then(() => res.status(200).json({message: 'address removed from address list'}))
                .catch(err => console.log(err));
        } else {
            res.status(200).json({message: 'address not found in address list'})
        }
    })
});

module.exports = router;
